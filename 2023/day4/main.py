def part1(file: str):
    array = []
    with open(file) as f:
        for line in f:
            tmp = []
            for el in [i.split(" ") for i in line.split(":")[1].split("|")]:
                tmp.append([int(it) for it in el if len(it) > 0])
            array.append(tmp)

    total = 0
    for card in array:
        num_matches = len(set(card[0]) & set(card[1]))
        if num_matches > 0:
            total += 2 ** (num_matches - 1)

    return total


def part2(file: str):
    array = []
    with open(file) as f:
        for line in f:
            tmp = []
            for el in [i.split(" ") for i in line.split(":")[1].split("|")]:
                tmp.append([int(it) for it in el if len(it) > 0])
            array.append(tmp)

    sets = [1 for _ in array]
    for num_set, card in enumerate(array):
        num_matches = len(set(card[0]) & set(card[1]))
        for j in range(num_matches):
            sets[num_set + 1 + j] += sets[num_set]

    return sum(sets)


if __name__ == "__main__":
    assert part1("example.txt") == 13
    print(part1("input.txt"))

    assert part2("example.txt") == 30
    print(part2("input.txt"))
