MKDIR = mkdir
LN    = ln -s

LATEST_YEAR := $(lastword $(sort $(wildcard [0-9]*)))
LATEST_DAY  := $(patsubst day%,%,$(lastword $(sort $(wildcard day[0-9]*))))

ifeq ($(LATEST_YEAR),)
	NEW_YEAR := $(shell date -u +%Y)
else
	NEW_YEAR := $(shell echo $$(( $(LATEST_YEAR) + 1 )) )
endif

ifeq ($(LATEST_DAY),)
	NEW_DAY := 1
else
	NEW_DAY := $(shell echo $$(( $(LATEST_DAY) + 1 )) )
endif


all: $(if $(wildcard $(LATEST_YEAR)),create_year,create_day)

create_year:
	$(MKDIR) "$(NEW_YEAR)"
	cd "$(NEW_YEAR)" && $(LN) "../Makefile" .

create_day:
	$(MKDIR) "day$(NEW_DAY)"

recreate_makefile:
	cd "$(LATEST_YEAR)" && $(LN) "../Makefile" .
