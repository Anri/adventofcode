(** Get input *)
let from input =
  let ic = open_in input in
  (* Read a file *)
  let rec read_line acc =
    try
      let line = input_line ic in
      read_line (line :: acc)
    with
    | End_of_file ->
      close_in ic;
      acc
  in
  (* Lis une ligne et la découpe proprement *)
  let scan line = String.split_on_char ' ' line in
  (* Convertis les string en nombre *)
  let convert l = List.map int_of_string l in
  let list = List.map scan (read_line []) in
  List.map convert list |> List.rev
;;

let first data =
  let is_safe l =
    let order l =
      let sorted = List.fast_sort compare l in
      List.equal (fun a b -> a = b) l sorted
      || List.equal (fun a b -> a = b) l (List.rev sorted)
    in
    let rec adjacent = function
      | x :: y :: tl -> if Int.abs (x - y) > 3 then true else adjacent (y :: tl)
      | _ :: [] | [] -> false
    in
    order l && adjacent l
  in
  List.filter is_safe data |> List.length
;;

let second data =
  (* todo *)
  0
;;

let _ =
  let data = from "input.txt" in
  print_endline ("first: " ^ Int.to_string (first data));
  print_endline ("second: " ^ Int.to_string (second data))
;;
