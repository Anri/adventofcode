(** Get input *)
let from input =
  let ic = open_in input in
  (* Read a file *)
  let rec read_line acc =
    try
      let line = input_line ic in
      read_line (line :: acc)
    with
    | End_of_file ->
      close_in ic;
      acc
  in
  (* Split column into couples *)
  let scan line = Scanf.sscanf line "%d %d" (fun x y -> x, y) in
  (* Split couple into 2 lists *)
  let split (acc1, acc2) (a, b) = a :: acc1, b :: acc2 in
  let zip = List.map scan (read_line []) in
  List.fold_left split ([], []) zip
;;

let first a b =
  let sorted_a, sorted_b = List.fast_sort compare a, List.fast_sort compare b in
  let m = List.map2 (fun a b -> Int.abs (a - b)) sorted_a sorted_b in
  List.fold_left ( + ) 0 m
;;

let second a b =
  (* todo *)
  0
;;

let _ =
  let a, b = from "input.txt" in
  print_endline ("first: " ^ Int.to_string (first a b));
  print_endline ("second: " ^ Int.to_string (second a b))
;;
